<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TOPModélisme.com</title>
</head>
<body>
    <div class="page">
        <div class="titre">
            <img src="elements_graphiques/img/logo_200px.gif" alt="Logo du Site" class="logo">
            <h1>
            le leader du modélisme en ligne
            </h1>
        </div>
        
        <div class="authentification">
            <form method="POST" action="page.php">
                <p>
                    <br>
                    <label for="email"></label>adresse mail</label>
                    <br>
                    <input type="email" id="email" name="email">
                    <br>
                    <label for="password"></label>mot de passe</label>
                    <br>
                    <input type="password" id="password" name="password">
                    <br>
                    <br>
                </p>

                <p>
                    <div class="myButton">se connecter</div>
                    <div class="myButton">créer un compte</div>
                </p>
            </form>
        </div>
        
        <div class="contenu">
            <?php
                $db = mysqli_connect('localhost', 'venteenligne', 'IsImA_2021/%', 'venteenligne', 3307)
                    or die('Erreur SQL : '.mysqli_error($db));
                $db -> query ('SET NAMES UTF8');

                if (isset($_GET["id_famille"])) {

                    // affichages des articles

                    $sql = 'SELECT * FROM article WHERE id_famille ='.$_GET["id_famille"];
            
                    $result = $db -> query($sql)
                        or die('ErreurSQL : '.mysqli_error($db));

                    // bouton retour
                    echo '<div class="bouton">';
                        echo "<a href='index.php'><div class='myButton'>retour</div></a>";
                    echo "</div>";    

                    // articles
                    echo '<div class="affichage">';

                    if (isset($_GET['id_famille']))
                    {
                        $path = '?id_famille='.$_GET['id_famille'];
                    }
                    else
                    {
                        $path = '/';
                    }

                    while($data=mysqli_fetch_array($result))
                    {
                        echo "<div class='article'>";
                            echo "<img src='elements_graphiques/img_articles/".$data["image"]."' alt='".$data["libelle"]."' class='imageArticle'>";
                            echo "<div class='description'>";
                                echo "<h3 class='titreArticle'>".$data['libelle']."</h3>";
                                echo "<p class='detail'>".$data['detail']."</p>";
                                echo "<div class='commande'>";
                                    echo "<p class='prix'>".$data['prix_ttc']." €</p>";
                                    //On crée un form pour récupérer l'id en cas de commande
                                    echo "<form name='article' method='POST' action=$path>";
                                        echo "<button type='submit' name='Bouton' class='myButton' value=".$data['id'].">commander</button>";
                                    echo "</form>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    }
                    echo "</div>";
                }
                
                else {
                
                    // affichage des familles  

                    $sql = 'SELECT * FROM famille';

                    $result = $db -> query($sql)
                        or die('ErreurSQL : '.mysqli_error($db));

                    echo '<div class="affichage">';

                    while ($data = mysqli_fetch_array($result)) { ?>

                        <form name='famille' method='POST' action='?id=$_GET[id]'>
                            <a href="?id_famille=<?php echo $data["id"];?>">
                        </form>

                        <div class='famille'>
                            <img src="elements_graphiques/img_familles/<?php echo $data["image"];?>" alt=""> 
                            <br>
                            <p><?php echo '<p class="titreFamille">'.$data["libelle"].'</p>'?></p>
                        </div>
                        </a><?php
                    }    
                    echo '</div>';
                }
                                
            
                // Lorsqu'on appuie sur le bouton 'commander'
                if(isset($_POST['Bouton'])){

                    // Récupération de l'id de l'article à commander
                    $id_article=$_POST['Bouton'];

                    // Récupération des données concernant l'article
                    $sql_commande = 'SELECT a.id AS id_article, taux, prix_ttc FROM article AS a
                            JOIN tva AS t ON a.id_tva=t.id WHERE a.id='.$id_article.'';
                    
                    $result_commande = $db -> query($sql_commande)
                        or die('ErreurSQL : '.mysqli_error($db));

                    $data_commande=mysqli_fetch_array($result_commande);
                    
                    // Calcul du prix HT et du prix de la TVA
                    $taux_tva = $data_commande['taux'] / 100;
                    $prix_tva = $data_commande['prix_ttc'] * $taux_tva;
                    $prix_ht = $data_commande['prix_ttc'] - $prix_tva;

                    // On vérifie que l'article ne soit pas déjà dans le panier (et on récupère sa quantite)
                    $sql = 'SELECT id_article, quantite FROM panier_article WHERE id_article='.$id_article.'';
                    $result = $db -> query($sql)
                        or die('ErreurSQL : '.mysqli_error($db));

                    $data_check=mysqli_fetch_array($result);

                    if ($data_check == NULL) // Si il n'y est pas, on l'insère
                    {
                        // Requête d'insertion
                        $AjouterArticle="INSERT INTO panier_article (id_panier, id_article, quantite, prix_ht, prix_tva, prix_ttc) VALUES ('1', '$data_commande[id_article]', '1', '$prix_ht', '$prix_tva', '$data_commande[prix_ttc]')";
                        mysqli_query($db, $AjouterArticle) or die('Erreur SQL !'.$AjouterArticle.'<br>'.mysqli_error($db));
                    }
                    else // Si il y est déjà, on augmente la quantité de 1
                    {
                        $nouvelle_quantite=$data_check['quantite']+1;
                        // Requête d'incrémentation
                        $IncrementerArticle = "UPDATE panier_article SET quantite = '$nouvelle_quantite' WHERE id_article = '$id_article'";
                        mysqli_query($db, $IncrementerArticle) or die('Erreur SQL !'.$IncrementerArticle.'<br>'.mysqli_error($db));
                    }
                    
                }
                mysqli_close($db);

            ?> 
        </div>


        <div class="panier">
            <?php
                $db = mysqli_connect('localhost', 'venteenligne', 'IsImA_2021/%', 'venteenligne', 3307)
                    or die('Erreur SQL : '.mysqli_error($db));
                $db -> query ('SET NAMES UTF8');

                // On récupère les données du panier
                $sql = 'SELECT * FROM panier_article AS p
                        JOIN article AS a ON a.id=p.id_article WHERE id_panier = 1';
                $result = $db -> query($sql)
                    or die('ErreurSQL : '.mysqli_error($db));

                // Entête du panier
                echo '<div class="entetePanier">';
                    echo '<img src="elements_graphiques/img/panier.gif" alt="Panier" id="panierImage">';
                    echo '<p id="panierTexte">votre panier</p>';
                echo '</div>';

                $data=mysqli_fetch_array($result);

                if($data == NULL) // Si le panier est vide, on affiche qu'il est vide
                {
                    echo '<div id="panierContenu">';
                        echo '<p id="panierVideTexte">';
                            echo 'votre panier est vide';
                        echo '</p>';
                    echo '</div>';
                }
                else // Sinon, on affiche le detail de la commande
                {
                    $prix_total = 0;
                    echo "<div class='panierContenu'>";
                    do
                    {
                        $prix_article = $data['quantite'] * $data['prix_ttc'];
                        $prix_total += $prix_article;
                        echo "<div class='ligne _article'>";
                            echo "<div class='nom_article'>";
                                echo $data['libelle'];
                            echo"</div>";
                            echo "<div class='prix_article'>";
                                echo $data['quantite']." x ".$data['prix_ttc']." = ".$prix_article." €";
                            echo"</div>";
                        echo "</div>";
                    }
                    while($data=mysqli_fetch_array($result));
                    echo "</div>";

                    echo "<div class='panierTotal'>";
                        echo "TOTAL : ".$prix_total." €";
                    echo "</div>";
                }
                
                if (isset($_GET['id_famille']))
                {
                    $path = '?id_famille='.$_GET['id_famille'];
                }
                else
                {
                    $path = '/';
                }

                echo "<div class='panierBouton'>";
                    echo "<form name='vider' method='POST' action=$path>";
                        //On récupère la valeur 'vider' qui nous indique de vider le panier 1.
                        echo "<button type='submit' name='Vider' class='myButton' value='1'>vider panier</button>";
                    echo "</form>";
                    echo "<input type='button' class='myButton' value='commander'>";
                echo "</div>";
                    
                // Lorsqu'on appuie sur le bouton 'vider panier'
                if(isset($_POST['Vider'])){
                    $ViderPanier = 'DELETE FROM panier_article WHERE id_panier ='.$_POST['Vider'].';'; 
                    mysqli_query($db, $ViderPanier) or die('Erreur SQL !'.$ViderPanier.'<br>'.mysqli_error($db));
                    //header('Location: '.$path);
                }
            
                mysqli_close($db);
            ?>
        </div>

        <div class="pied">
            <p id="textPiedDePage">
            TOPModélisme.com est enregistre au R.C.S sous le numero 1234567890
            <br>
            13 avenue du Pre la Reine - 75007 PARIS
            </p>
        </div>
    </div>
</body>
</html>